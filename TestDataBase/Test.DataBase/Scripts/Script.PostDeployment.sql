﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

DECLARE 
		@id int,
		@text NVARCHAR(6),
		@text1 NVARCHAR(1)

-- ------------------- ЗАПОЛНЯЕМ ENUMS ------------------------
BEGIN 

/*
	--------------------	Genders ----------
*/

SET @id = 1
SET @text = N'Male'
SET @text1 = N'm'
IF NOT EXISTS (
	SELECT [Id]
	FROM [Genders]
	WHERE [Id] = @id
	)
	INSERT [Genders]
	([Id], [Name], [ShortName])
	VALUES (@id, @text, @text1)
ELSE 
	UPDATE [Genders]
	SET [Name] = @text, [ShortName] = @text1
	WHERE [Id] = @id

SET @id = 2
SET @text = N'Female'
SET @text1 = N'f'
IF NOT EXISTS (
	SELECT [Id]
	FROM [Genders]
	WHERE [Id] = @id
	)
	INSERT [Genders]
	([Id], [Name], [ShortName])
	VALUES (@id, @text, @text1)
ELSE 
	UPDATE [Genders]
	SET [Name] = @text, [ShortName] = @text1
	WHERE [Id] = @id

END


-- ------------------- ТЕСТОВЫЕ ДАННЫЕ ДЛЯ Persons - 10 человек, для пагинации по 5 на клиенте ------------------------
BEGIN 
	
	INSERT [Persons]
	(
		[FirstName],
		[LastName],
		[PersonalNumber],
		[Birthdate],
		[GenderId],
		[Salary]
	)
	VALUES 
	(
		N'Дмитрий',
		N'Комельков',
		N'12345678901',
		N'2000-01-01',
		1,
		2500		-- :)
	)

	INSERT [Persons]
	(
		[FirstName],
		[LastName],
		[PersonalNumber],
		[Birthdate],
		[GenderId],
		[Salary]
	)
	VALUES 
	(
		N'John',
		N'Smith',
		N'12345678901',
		N'2000-01-01',
		1,
		500		
	)

	INSERT [Persons]
	(
		[FirstName],
		[LastName],
		[PersonalNumber],
		[Birthdate],
		[GenderId],
		[Salary]
	)
	VALUES 
	(
		N'Светлана',
		N'Разина',
		N'12345678901',
		N'2000-01-01',
		2,
		1500		
	)


	INSERT [Persons]
	(
		[FirstName],
		[LastName],
		[PersonalNumber],
		[Birthdate],
		[GenderId],
		[Salary]
	)
	VALUES 
	(
		N'Сергей',
		N'TestMan',
		N'12345678901',
		N'2000-01-01',
		1,
		3000		
	)

	INSERT [Persons]
	(
		[FirstName],
		[LastName],
		[PersonalNumber],
		[Birthdate],
		[GenderId],
		[Salary]
	)
	VALUES 
	(
		N'Анна',
		N'TestWoman',
		N'12345678901',
		N'2000-01-01',
		2,
		1000		
	)

	INSERT [Persons]
	(
		[FirstName],
		[LastName],
		[PersonalNumber],
		[Birthdate],
		[GenderId],
		[Salary]
	)
	VALUES 
	(
		N'Сергей',
		N'TestMan',
		N'12345678901',
		N'2000-01-01',
		1,
		3000		
	)

	INSERT [Persons]
	(
		[FirstName],
		[LastName],
		[PersonalNumber],
		[Birthdate],
		[GenderId],
		[Salary]
	)
	VALUES 
	(
		N'Анна',
		N'TestWoman',
		N'12345678901',
		N'2000-01-01',
		2,
		1000		
	)

	INSERT [Persons]
	(
		[FirstName],
		[LastName],
		[PersonalNumber],
		[Birthdate],
		[GenderId],
		[Salary]
	)
	VALUES 
	(
		N'Сергей',
		N'TestMan',
		N'12345678901',
		N'2000-01-01',
		1,
		3000		
	)

	INSERT [Persons]
	(
		[FirstName],
		[LastName],
		[PersonalNumber],
		[Birthdate],
		[GenderId],
		[Salary]
	)
	VALUES 
	(
		N'Анна',
		N'TestWoman',
		N'12345678901',
		N'2000-01-01',
		2,
		1000		
	)

	INSERT [Persons]
	(
		[FirstName],
		[LastName],
		[PersonalNumber],
		[Birthdate],
		[GenderId],
		[Salary]
	)
	VALUES 
	(
		N'Сергей',
		N'TestMan',
		N'12345678901',
		N'2000-01-01',
		1,
		3000		
	)

	INSERT [Persons]
	(
		[FirstName],
		[LastName],
		[PersonalNumber],
		[Birthdate],
		[GenderId],
		[Salary]
	)
	VALUES 
	(
		N'Анна',
		N'TestWoman',
		N'12345678901',
		N'2000-01-01',
		2,
		1000		
	)

	INSERT [Persons]
	(
		[FirstName],
		[LastName],
		[PersonalNumber],
		[Birthdate],
		[GenderId],
		[Salary]
	)
	VALUES 
	(
		N'Сергей',
		N'TestMan',
		N'12345678901',
		N'2000-01-01',
		1,
		3000		
	)

	INSERT [Persons]
	(
		[FirstName],
		[LastName],
		[PersonalNumber],
		[Birthdate],
		[GenderId],
		[Salary]
	)
	VALUES 
	(
		N'Анна',
		N'TestWoman',
		N'12345678901',
		N'2000-01-01',
		2,
		1000		
	)

	INSERT [Persons]
	(
		[FirstName],
		[LastName],
		[PersonalNumber],
		[Birthdate],
		[GenderId],
		[Salary]
	)
	VALUES 
	(
		N'Сергей',
		N'TestMan',
		N'12345678901',
		N'2000-01-01',
		1,
		3000		
	)

	INSERT [Persons]
	(
		[FirstName],
		[LastName],
		[PersonalNumber],
		[Birthdate],
		[GenderId],
		[Salary]
	)
	VALUES 
	(
		N'Анна',
		N'TestWoman',
		N'12345678901',
		N'2000-01-01',
		2,
		1000		
	)


END