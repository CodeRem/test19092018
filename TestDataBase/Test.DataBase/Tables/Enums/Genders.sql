﻿CREATE TABLE [dbo].[Genders]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[Name] NVARCHAR(6) NOT NULL,		--male/female
	[ShortName] NVARCHAR(1) NOT NULL,	-- m/f
)
