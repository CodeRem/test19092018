﻿CREATE TABLE [dbo].[Persons]
(
	[Id] INT IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[FirstName] NVARCHAR(25) NOT NULL,
	[LastName] NVARCHAR(50) NOT NULL,
	[PersonalNumber] NVARCHAR(11) NOT NULL,
	[Birthdate] DATE NOT NULL,
	[GenderId] INT NOT NULL,
	[Salary] MONEY NOT NULL,

	CONSTRAINT [FK_Persons_Genders_GenderId] FOREIGN KEY ([GenderId]) REFERENCES [dbo].[Genders] ([Id])
)
