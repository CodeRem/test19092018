import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AppStore} from '../_app-store/app-store';
import {select, Store} from '@ngrx/store';
import {Router} from '@angular/router';
import {BehaviorSubject, Subject} from 'rxjs';
import {AppNotificationModel} from '../shared/notifications/models/app-notification-model';
import {map, takeUntil} from 'rxjs/operators';
import {MatSidenav} from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  @ViewChild('sidenav') sidenav: MatSidenav;

  reason = '';

  // для компонента отображения нотификаций
  appNotifications$: BehaviorSubject<AppNotificationModel[]> = new BehaviorSubject<AppNotificationModel[]>(null);

  // для компонента отображения лоадера
  showLoader$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  // для убиваиня подписок
  private readonly ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private router: Router,
    private readonly store: Store<AppStore>,
  ) {
  }

  ngOnInit(): void {
    // подписка
    this.store.pipe(select(x => x.sharedStore.notificationStore.newNotifications))
      .pipe(takeUntil(this.ngUnsubscribe)
      )
      .subscribe(
        (res: AppNotificationModel[]) => {
          // console.log('AppComponent, res=', res);
          this.appNotifications$.next(res);
        }
      );

    // подписка на отображение спиннера при нттп запросах модуля авторизации
    this.store.pipe(select(x => x.sharedStore.counterHttpRequestsInActiveStore.count))
      .pipe(
        takeUntil(this.ngUnsubscribe),
        map(x => x > 0)
      )
      .subscribe(
        (res: boolean) => {
          this.showLoader$.next(res);
        }
      );

  }

  close(reason: string) {
    this.reason = reason;
    this.sidenav.close();
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
