import {ActionReducerMap} from '@ngrx/store';
import {SharedReducer} from '../shared/core/shared-store/shared-reducer';
import {PersonsReducer} from '../modules/persons/store/persons-reducer';

export const AppReducer: ActionReducerMap<any> = {
  sharedStore: SharedReducer,
  personsStore: PersonsReducer
};
