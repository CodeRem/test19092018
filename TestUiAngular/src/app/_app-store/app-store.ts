import {SharedStore} from '../shared/core/shared-store/shared-store';
import {PersonsStore} from '../modules/persons/store/persons-store';


export class AppStore {
  sharedStore: SharedStore = new SharedStore();
  personsStore: PersonsStore = new PersonsStore();
}


