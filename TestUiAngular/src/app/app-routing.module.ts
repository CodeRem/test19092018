import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

const appRoutes: Routes = [
  // лэйзи лоадинг модули
  {
    path: 'persons',
    loadChildren: './modules/persons/persons.module#PersonsModule'
  },

  // кэчи для роутов
  {
    path: '**',
    redirectTo: 'persons',
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes
      // , { enableTracing: true }
    )
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
