import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './_app-component/app.component';
import {NotificationsModule} from './shared/notifications/notifications.module';
import {CoreModule} from './shared/core/core.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {WidgetsModule} from './shared/widgets/widgets.module';
import {AppReducer} from './_app-store/app-reducer';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {AppRoutingModule} from './app-routing.module';
import {AppStore} from './_app-store/app-store';
import {PersonsModule} from './modules/persons/persons.module';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    CoreModule,
    NotificationsModule,
    WidgetsModule,
    StoreModule.forRoot(AppReducer, {initialState: new AppStore()}),

    // todo удалить перед продакшин, чтоб не мешалось
    StoreDevtoolsModule.instrument({
      maxAge: 50, // Retains last 50 states
      // logOnly: environment.production // Restrict extension to log-only mode
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
