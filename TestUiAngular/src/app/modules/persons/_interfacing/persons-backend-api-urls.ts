export const PersonsBackendApiUrls = {
  baseHostUrl:
    'http://localhost:50348',
  // 'http://localhost:5000',

  // -- prod
  // baseHostUrl: "http://personalcapitalassistant.com",

  folderUrl: '/api', // путь для виртуальной директории веб сервера (папка куда публикуется сайт апи)

  // конечная часть урла - {контроллер}/{action}
  getPersons: '/Persons/GetListPersons',
  createPersons: '/Persons/CreatePerson',
  updatePersons: '/Persons/UpdatePerson',
  deletePerson: '/Persons/DeletePerson',

  getGenders: '/Gender/GetListGenders'
};
