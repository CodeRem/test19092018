import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource} from '@angular/material';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {PersonViewModel} from '../../../../shared/core/models-backend-end-point/person-view-model';
import {PersonUpdateViewModel} from '../../../../shared/core/models-backend-end-point/person-update-view-model';
import {PersonCreateViewModel} from '../../../../shared/core/models-backend-end-point/person-create-view-model';
import {GendersEnum} from '../../../../shared/core/models-backend-end-point/Enums/genders-enum';
import {GenderViewModel} from '../../../../shared/core/models-backend-end-point/gender-view-model';
import {takeUntil} from 'rxjs/operators';
import {GendersService} from '../../services/genders.service';
import {select, Store} from '@ngrx/store';
import {AppStore} from '../../../../_app-store/app-store';

@Component({
  selector: 'app-create-update-person',
  templateUrl: './create-update-person.component.html',
  styleUrls: ['./create-update-person.component.scss']
})
export class CreateUpdatePersonComponent implements OnInit, OnDestroy {


  // personUpdate: PersonUpdateViewModel = null;
  // personCreate: PersonCreateViewModel = null;

  person: PersonUpdateViewModel = null;
  listGenders: GenderViewModel[] = null;

  pageTitle: string = null;
  saveButtonLabel: string = null;

  // region FormControls

  firstNameControl: FormControl;
  lastNameControl: FormControl;
  personalNumberControl: FormControl;
  birthdateControl: FormControl;
  salaryControl: FormControl;
  selectGender: FormControl;

  // endregion

  private readonly ngUnsubscribe: Subject<void> = new Subject<void>();


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialogRef<CreateUpdatePersonComponent>,
    private fb: FormBuilder,
    private store: Store<AppStore>,
    private gendersService: GendersService
  ) {
  }

  ngOnInit() {

    this.store.pipe(select(x => x.personsStore.listGenders))
      .pipe(takeUntil(this.ngUnsubscribe),
      )
      .subscribe(
        (x: GenderViewModel[]) => {
          // если данных нет (налл) - дергаем сервис на получение
          if (!x) {
            this.gendersService.getListGenders();
          } else {
            this.listGenders = x;
          }
        }
      );


    if (!this.data.person) {
      this.pageTitle = 'Добавление персоны';
      this.saveButtonLabel = 'Создать';
      this.person = {
        id: 0,
        firstName: '',
        lastName: '',
        personalNumber: '',
        birthdate: null,
        genderId: this.listGenders[0].id,
        salary: null
      };
    } else {
      this.pageTitle = 'Редактирование персоны';
      this.saveButtonLabel = 'Сохранить';
      this.person = {
        id: this.data.person.id,
        firstName: this.data.person.firstName,
        lastName: this.data.person.lastName,
        personalNumber: this.data.person.personalNumber,
        birthdate: this.data.person.birthdate,
        genderId: this.data.person.gender.id,
        salary: this.data.person.salary
      };
    }

    this.firstNameControl = this.fb.control(this.person.firstName, [Validators.required]);
    this.lastNameControl = this.fb.control(this.person.lastName, [Validators.required]);
    this.personalNumberControl = this.fb.control(this.person.personalNumber, [Validators.required, Validators.minLength(11), Validators.maxLength(11)]);
    this.birthdateControl = this.fb.control(this.person.birthdate, [Validators.required]);
    this.salaryControl = this.fb.control(this.person.salary, [Validators.required, Validators.min(0)]);

    this.valueChanges();
  }

  valueChanges() {
    this.firstNameControl.valueChanges
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (selectValue: string) => {
          this.person.firstName = selectValue;
        }
      );

    this.lastNameControl.valueChanges
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (selectValue: string) => {
          this.person.lastName = selectValue;
        }
      );

    this.personalNumberControl.valueChanges
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (selectValue: string) => {
          this.person.personalNumber = selectValue;
        }
      );

    this.birthdateControl.valueChanges
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (selectValue: Date) => {
          this.person.birthdate = selectValue;
        }
      );

    this.salaryControl.valueChanges
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (selectValue: number) => {
          this.person.salary = selectValue;
        }
      );

  }

  onlyDigits(event): boolean {
    // return (event.charCode === 8 || event.charCode === 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onlyDecimal(key) {

    // getting key code of pressed key
    const keycode = (key.which) ? key.which : key.keyCode;
    // comparing pressed keycodes

    if (keycode > 31 && (keycode < 48 || keycode > 57) && keycode !== 46) {
      return false;
    }
    return true;
  }

  save() {
    // console.log('this.person', this.person);

    this.dialog.close(this.person);
  }

  cancel() {
    this.dialog.close();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
