import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {PersonsService} from '../../services/persons.service';
import {select, Store} from '@ngrx/store';
import {AppStore} from '../../../../_app-store/app-store';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {Subject} from 'rxjs';
import {PersonViewModel} from '../../../../shared/core/models-backend-end-point/person-view-model';
import {filter, takeUntil} from 'rxjs/operators';
import {CreateUpdatePersonComponent} from '../create-update-person/create-update-person.component';
import {GendersService} from '../../services/genders.service';
import {PersonUpdateViewModel} from '../../../../shared/core/models-backend-end-point/person-update-view-model';


@Component({
  selector: 'app-list-persons',
  templateUrl: './list-persons.component.html',
  styleUrls: ['./list-persons.component.scss']
})
export class ListPersonsComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  listPersons: PersonViewModel[] = [];

  dataSource = new MatTableDataSource();

  dateFormat = 'dd.MM.yyyy';
  numberFormat = '4.0-5';

  displayedColumns = [
    'firstName'
    , 'lastName'
    , 'personalNumber'
    , 'birthdate'
    , 'gender'
    , 'salary'
    , 'buttons'
  ];

  private readonly ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private personsService: PersonsService,
              private gendersService: GendersService,
              private store: Store<AppStore>,
              public dialog: MatDialog) {
  }


  ngOnInit() {
    this.store.pipe(select(x => x.personsStore.listPersons))
      .pipe(takeUntil(this.ngUnsubscribe),
      )
      .subscribe(
        (x: PersonViewModel[]) => {
          // если данных нет (налл) - дергаем сервис на получение
          if (!x) {
            this.personsService.getListPersons();
          } else {
            this.listPersons = x;
            this.dataSource = new MatTableDataSource<PersonViewModel>(x);
            this.dataSource.paginator = this.paginator;
          }
        }
      );

    this.gendersService.getListGenders();
  }

  // отработка нажатия клавишь на поле Фильтр (фильтрация)
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  editItem(person: PersonViewModel) {
    const dialogRef = this.dialog.open(CreateUpdatePersonComponent, {
      width: '250px',
      data: {person: person}
    });

    dialogRef.afterClosed()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (p: PersonUpdateViewModel) => {

          this.personsService.updatePerson(p);

        }
      );
  }

  addPerson() {
    const dialogRef = this.dialog.open(CreateUpdatePersonComponent, {
      width: '250px',
      data: {person: null}
    });

    dialogRef.afterClosed()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (p: PersonUpdateViewModel) => {

          this.personsService.createPerson(p);

        }
      );
  }


  deleteItem(item: PersonViewModel) {
    this.personsService.deletePerson(item.id);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
