import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ListPersonsComponent} from './components/list-persons/list-persons.component';

const personsRoutes: Routes = [
  {
    path: '',
    component: ListPersonsComponent,

  },
];

@NgModule({
  imports: [RouterModule.forChild(personsRoutes)],
  exports: [RouterModule]
})
export class PersonsRoutingModule {
}
