import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PersonsRoutingModule} from './persons-routing.module';
import { ListPersonsComponent } from './components/list-persons/list-persons.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {WidgetsModule} from '../../shared/widgets/widgets.module';
import { CreateUpdatePersonComponent } from './components/create-update-person/create-update-person.component';
import {CoreModule} from '../../shared/core/core.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule
    , FormsModule
    , ReactiveFormsModule
    , PersonsRoutingModule
    , HttpClientModule
    , WidgetsModule
    , CoreModule
  ],
  declarations: [ListPersonsComponent, CreateUpdatePersonComponent],
  entryComponents: [
    CreateUpdatePersonComponent
  ]
})
export class PersonsModule { }
