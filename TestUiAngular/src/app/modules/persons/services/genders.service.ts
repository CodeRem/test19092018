import { Injectable } from '@angular/core';
import {PersonsBackendApiUrls} from '../_interfacing/persons-backend-api-urls';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {AppStore} from '../../../_app-store/app-store';
import {
  EndHttpRequest,
  StartHttpRequest
} from '../../../shared/core/shared-store/store-items/counter-http-requests-in-active/counter-http-requests-in-active-actions';
import {ErrorHttpRequestNotification} from '../../../shared/core/shared-notifications/error-http-request.notification';
import {ListGendersHttpResponseSuccessAction} from '../store/store-items/list-genders/list-genders-actions';
import {GenderViewModel} from '../../../shared/core/models-backend-end-point/gender-view-model';

@Injectable({
  providedIn: 'root'
})
export class GendersService {

  baseUrl: string = PersonsBackendApiUrls.baseHostUrl + PersonsBackendApiUrls.folderUrl;

  constructor(private readonly http: HttpClient,
              private store: Store<AppStore>) {
  }

  getListGenders(actionAfterSuccess?, actionAfterError?) {
    this.store.dispatch(new StartHttpRequest(PersonsBackendApiUrls.getGenders));
    this.http.get<GenderViewModel[]>(`${this.baseUrl}${PersonsBackendApiUrls.getGenders}`)
      .subscribe(
        (res) => {
          this.store.dispatch(new EndHttpRequest(PersonsBackendApiUrls.getGenders));
          this.store.dispatch(new ListGendersHttpResponseSuccessAction(PersonsBackendApiUrls.getGenders, res));
          if (actionAfterSuccess) {
            actionAfterSuccess();
          }
        },
        (err) => {
          this.store.dispatch(new EndHttpRequest(PersonsBackendApiUrls.getGenders));
          this.store.dispatch(ErrorHttpRequestNotification);
          if (actionAfterError) {
            actionAfterError();
          }
        }
      );
  }
}
