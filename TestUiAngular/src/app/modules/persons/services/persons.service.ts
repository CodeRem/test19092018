import { Injectable } from '@angular/core';
import {AppStore} from '../../../_app-store/app-store';
import {Store} from '@ngrx/store';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {
  EndHttpRequest,
  StartHttpRequest
} from '../../../shared/core/shared-store/store-items/counter-http-requests-in-active/counter-http-requests-in-active-actions';
import {ErrorHttpRequestNotification} from '../../../shared/core/shared-notifications/error-http-request.notification';
import {PersonsBackendApiUrls} from '../_interfacing/persons-backend-api-urls';
import {PersonViewModel} from '../../../shared/core/models-backend-end-point/person-view-model';
import {NotificationModel, NotificationTypes} from '../../../shared/core/shared-store/store-items/notifications/notification-store';
import {ListPersonsHttpResponseSuccessAction} from '../store/store-items/list-persons/list-persons-actions';
import {AddNotificationsAction} from '../../../shared/core/shared-store/store-items/notifications/notification-actions';
import {PersonUpdateViewModel} from '../../../shared/core/models-backend-end-point/person-update-view-model';

@Injectable({
  providedIn: 'root'
})
export class PersonsService {

  headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  baseUrl: string = PersonsBackendApiUrls.baseHostUrl + PersonsBackendApiUrls.folderUrl;

  constructor(private readonly http: HttpClient,
              private store: Store<AppStore>) {
  }

  getListPersons(actionAfterSuccess?, actionAfterError?) {
    this.store.dispatch(new StartHttpRequest(PersonsBackendApiUrls.getPersons));
    this.http.get<PersonViewModel[]>(`${this.baseUrl}${PersonsBackendApiUrls.getPersons}`)
      .subscribe(
        (res) => {
          this.store.dispatch(new EndHttpRequest(PersonsBackendApiUrls.getPersons));
          this.store.dispatch(new ListPersonsHttpResponseSuccessAction(PersonsBackendApiUrls.getPersons, res));
          if (actionAfterSuccess) {
            actionAfterSuccess();
          }
        },
        (err) => {
          this.store.dispatch(new EndHttpRequest(PersonsBackendApiUrls.getPersons));
          this.store.dispatch(ErrorHttpRequestNotification);
          if (actionAfterError) {
            actionAfterError();
          }
        }
      );
  }

  createPerson(person: PersonUpdateViewModel) {
    this.store.dispatch(new StartHttpRequest(PersonsBackendApiUrls.createPersons));
    this.http.post(`${this.baseUrl}${PersonsBackendApiUrls.createPersons}`, JSON.stringify(person), {headers: this.headers})
      .subscribe(
        () => {
          this.store.dispatch(new EndHttpRequest(PersonsBackendApiUrls.createPersons));
          this.store.dispatch(new AddNotificationsAction([ new NotificationModel('Создание', 'Создание персоны прошло успешно.', NotificationTypes.Info)]));
          this.getListPersons();
        },
        () => {
          this.store.dispatch(new EndHttpRequest(PersonsBackendApiUrls.createPersons));
          this.store.dispatch(ErrorHttpRequestNotification);
        }
      );
  }

  updatePerson(person: PersonUpdateViewModel) {
    this.store.dispatch(new StartHttpRequest(PersonsBackendApiUrls.updatePersons));
    this.http.put(`${this.baseUrl}${PersonsBackendApiUrls.updatePersons}`, JSON.stringify(person), {headers: this.headers})
      .subscribe(
        () => {
          this.store.dispatch(new EndHttpRequest(PersonsBackendApiUrls.updatePersons));
          this.store.dispatch(new AddNotificationsAction([ new NotificationModel('Редактировани', 'Запись обновлена успешно.', NotificationTypes.Info)]));
          this.getListPersons();
        },
        () => {
          this.store.dispatch(new EndHttpRequest(PersonsBackendApiUrls.updatePersons));
          this.store.dispatch(ErrorHttpRequestNotification);
        }
      );
  }

  deletePerson(id: number, actionAfterSuccess?, actionAfterError?) {
    this.store.dispatch(new StartHttpRequest(PersonsBackendApiUrls.deletePerson));
    this.http.delete(`${this.baseUrl}${PersonsBackendApiUrls.deletePerson}/${id}`)
      .subscribe(
        (x) => {
          this.store.dispatch(new EndHttpRequest(PersonsBackendApiUrls.deletePerson));
          // console.log('x', x);
          this.store.dispatch(new AddNotificationsAction([ new NotificationModel('Удаление', 'Удаление записи прошло успешно.', NotificationTypes.Info)]));
          this.getListPersons();
          if (actionAfterSuccess) {
            actionAfterSuccess();
          }
        },
        () => {
          this.store.dispatch(new EndHttpRequest(PersonsBackendApiUrls.deletePerson));
          this.store.dispatch(ErrorHttpRequestNotification);
          if (actionAfterError) {
            actionAfterError();
          }
        }
      );
  }
}
