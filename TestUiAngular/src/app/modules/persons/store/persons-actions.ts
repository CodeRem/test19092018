import {ListPersonsActions} from './store-items/list-persons/list-persons-actions';
import {ListGendersActions} from './store-items/list-genders/list-genders-actions';


export type PersonsActions = ListPersonsActions | ListGendersActions;
