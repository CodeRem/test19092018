import {PersonsStore} from './persons-store';
import {PersonsActions} from './persons-actions';
import {ListPersonsReducer} from './store-items/list-persons/list-persons-reducer';
import {ListGendersReducer} from './store-items/list-genders/list-genders-reducer';


export function PersonsReducer (store: PersonsStore, action: PersonsActions): PersonsStore {
  return {
    listPersons: ListPersonsReducer(store.listPersons, action),
    listGenders: ListGendersReducer(store.listGenders, action)
  };
}
