import {PersonViewModel} from '../../../shared/core/models-backend-end-point/person-view-model';
import {GenderViewModel} from '../../../shared/core/models-backend-end-point/gender-view-model';

export class PersonsStore {
  constructor(
    public listPersons: PersonViewModel[] = null,
    public listGenders: GenderViewModel[] = null
  ) {
  }
}
