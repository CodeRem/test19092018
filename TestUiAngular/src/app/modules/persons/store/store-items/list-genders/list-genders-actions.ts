import {Action} from '@ngrx/store';
import {nameofObject} from '../../../../../shared/core/functions/nameof-operators';
import {PersonViewModel} from '../../../../../shared/core/models-backend-end-point/person-view-model';
import {GenderViewModel} from '../../../../../shared/core/models-backend-end-point/gender-view-model';


export class ListGendersHttpResponseSuccessAction implements Action {
  readonly type: string = nameofObject(this);
  public payload?: any;

  constructor(httpRequestUrl: string, listGenders: GenderViewModel[]) {
    this.payload = {
      httpRequestUrl: httpRequestUrl,
      listGenders: listGenders
    };
  }
}

export type ListGendersActions = ListGendersHttpResponseSuccessAction;
