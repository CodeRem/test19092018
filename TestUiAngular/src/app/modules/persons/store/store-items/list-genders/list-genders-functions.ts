import * as _ from 'lodash';
import {GenderViewModel} from '../../../../../shared/core/models-backend-end-point/gender-view-model';
import {ListGendersHttpResponseSuccessAction} from './list-genders-actions';

export function handleListGendersHttpResponseSuccessAction(store: GenderViewModel[], action: ListGendersHttpResponseSuccessAction): GenderViewModel[] {
  return _.cloneDeep(action.payload.listGenders);
}
