
import {nameofObject} from '../../../../../shared/core/functions/nameof-operators';
import {GenderViewModel} from '../../../../../shared/core/models-backend-end-point/gender-view-model';
import {ListGendersActions, ListGendersHttpResponseSuccessAction} from './list-genders-actions';
import {handleListGendersHttpResponseSuccessAction} from './list-genders-functions';

export function ListGendersReducer (store: GenderViewModel[], action: ListGendersActions): GenderViewModel[] {

  switch (action.type) {
    case  nameofObject(new ListGendersHttpResponseSuccessAction('', [])):
      return handleListGendersHttpResponseSuccessAction(store, action);

    default:
      return store;
  }
}
