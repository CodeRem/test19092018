import {Action} from '@ngrx/store';
import {nameofObject} from '../../../../../shared/core/functions/nameof-operators';
import {PersonViewModel} from '../../../../../shared/core/models-backend-end-point/person-view-model';


export class ListPersonsHttpResponseSuccessAction implements Action {
  readonly type: string = nameofObject(this);
  public payload?: any;

  constructor(httpRequestUrl: string, listPersons: PersonViewModel[]) {
    this.payload = {
      httpRequestUrl: httpRequestUrl,
      listPersons: listPersons
    };
  }
}

export type ListPersonsActions = ListPersonsHttpResponseSuccessAction;
