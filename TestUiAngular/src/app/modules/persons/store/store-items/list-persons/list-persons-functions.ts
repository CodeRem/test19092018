import * as _ from 'lodash';
import {PersonViewModel} from '../../../../../shared/core/models-backend-end-point/person-view-model';
import {ListPersonsHttpResponseSuccessAction} from './list-persons-actions';

export function handleListPersonsHttpResponseSuccessAction(store: PersonViewModel[], action: ListPersonsHttpResponseSuccessAction): PersonViewModel[] {
  return _.cloneDeep(action.payload.listPersons);
}

