import {PersonViewModel} from '../../../../../shared/core/models-backend-end-point/person-view-model';
import {PersonsActions} from '../../persons-actions';
import {nameofObject} from '../../../../../shared/core/functions/nameof-operators';
import {ListPersonsHttpResponseSuccessAction} from './list-persons-actions';
import {handleListPersonsHttpResponseSuccessAction} from './list-persons-functions';

export function ListPersonsReducer (store: PersonViewModel[], action: PersonsActions): PersonViewModel[] {

  switch (action.type) {
    case  nameofObject(new ListPersonsHttpResponseSuccessAction('', [])):
      return handleListPersonsHttpResponseSuccessAction(store, action);

    default:
      return store;
  }
}
