import {Store} from '@ngrx/store';
import {take} from 'rxjs/operators';

// получения стора синхронно, чтоб посмотреть, что там
export const GetState = <T>(store: Store<T>) => {
  let state: T;

  store.pipe(take(1)).subscribe(s => state = s);

  return state;
};
