export const nameofObjectProperty = <T>(
  name: keyof T
) => name;

export const nameofObject = (item: Object): string => {
  return item.constructor.name;
};
