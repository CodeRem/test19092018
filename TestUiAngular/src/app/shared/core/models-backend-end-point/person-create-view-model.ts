﻿import {GendersEnum} from './Enums/genders-enum';


export class PersonCreateViewModel   {
	public firstName: string;
	public lastName: string;
	public personalNumber: string;
	public birthdate: Date;
	public genderId: GendersEnum;
	public salary: number;
}
