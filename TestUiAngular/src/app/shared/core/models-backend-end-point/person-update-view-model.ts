﻿import {GendersEnum} from './Enums/genders-enum';


export class PersonUpdateViewModel   {
	public id: number;
	public firstName: string;
	public lastName: string;
	public personalNumber: string;
	public birthdate: Date;
	public genderId: GendersEnum;
	public salary: number;
}
