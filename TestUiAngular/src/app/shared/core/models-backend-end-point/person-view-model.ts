﻿

import { GenderViewModel } from './gender-view-model';

export class PersonViewModel   {
	public id: number;
	public firstName: string;
	public lastName: string;
	public personalNumber: string;
	public birthdate: Date;
	public gender: GenderViewModel;
	public salary: number;
}