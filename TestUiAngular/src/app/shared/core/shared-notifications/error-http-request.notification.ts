import {AddNotificationsAction} from '../shared-store/store-items/notifications/notification-actions';
import {NotificationModel, NotificationTypes} from '../shared-store/store-items/notifications/notification-store';

export const ErrorHttpRequestNotification = new AddNotificationsAction([
  new NotificationModel('Ошибка', 'Ошибка запроса на получение данных.', NotificationTypes.Error)
]);
