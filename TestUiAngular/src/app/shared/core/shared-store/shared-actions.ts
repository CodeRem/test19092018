import {CounterHttpRequestsInActiveActions} from './store-items/counter-http-requests-in-active/counter-http-requests-in-active-actions';
import {NotificationActions} from './store-items/notifications/notification-actions';


export type SharedActions =
  CounterHttpRequestsInActiveActions
  | NotificationActions;
