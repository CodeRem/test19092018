import {SharedStore} from './shared-store';
import {SharedActions} from './shared-actions';
import {CounterHttpRequestsInActiveReducer} from './store-items/counter-http-requests-in-active/counter-http-requests-in-active-reducer';
import {NotificationReducer} from './store-items/notifications/notification-reducer';

export function SharedReducer(store: SharedStore, action: SharedActions): SharedStore {
  return {
    counterHttpRequestsInActiveStore: CounterHttpRequestsInActiveReducer(store.counterHttpRequestsInActiveStore, action),
    notificationStore: NotificationReducer(store.notificationStore, action),
  };
}
