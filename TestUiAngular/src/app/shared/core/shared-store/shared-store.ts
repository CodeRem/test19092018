import {CounterHttpRequestsInActiveStore} from './store-items/counter-http-requests-in-active/counter-http-requests-in-active-store';
import {NotificationStore} from './store-items/notifications/notification-store';

export class SharedStore {
  constructor(
    public counterHttpRequestsInActiveStore: CounterHttpRequestsInActiveStore = new CounterHttpRequestsInActiveStore(),
    public notificationStore: NotificationStore = new NotificationStore(),
  ) {}
}
