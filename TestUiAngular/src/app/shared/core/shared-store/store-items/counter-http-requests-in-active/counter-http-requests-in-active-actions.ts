import {Action} from '@ngrx/store';
import {nameofObject} from '../../../functions/nameof-operators';

export class EndHttpRequest implements Action {
  readonly type: string = nameofObject(this); // 'EndHttpRequest';
  public payload?: any;

  constructor(httpRequestUrl: string) {
    this.payload = {
      httpRequestUrl: httpRequestUrl
    };
  }
}

export class StartHttpRequest implements Action {
  readonly type: string = nameofObject(this); // 'StartHttpRequest';
  public payload?: any;

  constructor(httpRequestUrl: string) {
    this.payload = {
      httpRequestUrl: httpRequestUrl
    };
  }
}


export type CounterHttpRequestsInActiveActions =
  EndHttpRequest
  | StartHttpRequest;

