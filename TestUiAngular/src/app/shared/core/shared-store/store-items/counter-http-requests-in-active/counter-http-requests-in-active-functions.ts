import {CounterHttpRequestsInActiveStore} from './counter-http-requests-in-active-store';
import {StartHttpRequest} from './counter-http-requests-in-active-actions';
import * as _ from 'lodash';


export function requestsInProgressIncrement(store: CounterHttpRequestsInActiveStore, action: StartHttpRequest): CounterHttpRequestsInActiveStore {

  const arrayRequestNames: Array<string> = _.cloneDeep(store.requestNames);

  arrayRequestNames.push(action.payload.httpRequestUrl);

  return new CounterHttpRequestsInActiveStore(store.count + 1, arrayRequestNames);

}

export function requestsInProgressDecrement(store: CounterHttpRequestsInActiveStore, action: StartHttpRequest): CounterHttpRequestsInActiveStore {
  const arrayRequestNames: Array<string> = _.cloneDeep(store.requestNames);

  // переменная для индекса искомого элемента в массиве
  let index = -1;

  // находим индекс
  for (let i = 0; i < arrayRequestNames.length; i++) {

    if (arrayRequestNames[i] === action.payload.httpRequestUrl) {
      index = i;
    }
  }

  if (index >= 0) {
    arrayRequestNames.splice(index, 1);
  }

  return new CounterHttpRequestsInActiveStore(store.count - 1, arrayRequestNames);
}
