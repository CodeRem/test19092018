import {CounterHttpRequestsInActiveStore} from './counter-http-requests-in-active-store';
import {
  CounterHttpRequestsInActiveActions,
  EndHttpRequest,
  StartHttpRequest
} from './counter-http-requests-in-active-actions';
import {requestsInProgressDecrement, requestsInProgressIncrement} from './counter-http-requests-in-active-functions';
import {nameofObject} from '../../../functions/nameof-operators';

export function CounterHttpRequestsInActiveReducer(store: CounterHttpRequestsInActiveStore, action: CounterHttpRequestsInActiveActions): CounterHttpRequestsInActiveStore {
  switch (action.type) {

    case nameofObject(new StartHttpRequest('')): // 'StartHttpRequest':
      return requestsInProgressIncrement(store, action);

    case nameofObject(new EndHttpRequest('')):  // 'EndHttpRequest':
      return requestsInProgressDecrement(store, action);

    default:
      return store;
  }
}
