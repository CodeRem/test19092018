export class CounterHttpRequestsInActiveStore {
  constructor(
    public count: number = 0,
    public requestNames: string[] = []
  ) {}
}
