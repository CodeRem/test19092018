import {Action} from '@ngrx/store';
import {NotificationModel} from './notification-store';
import {nameofObject} from '../../../functions/nameof-operators';

export class AddNotificationsAction implements Action {
  readonly type: string = nameofObject(this);
  public payload?: any;

  constructor(notifications: NotificationModel[]) {
    if (notifications && notifications.length > 0) {
      this.payload = {
        notifications: notifications
      };
    }
  }
}

export type NotificationActions =
  AddNotificationsAction;

