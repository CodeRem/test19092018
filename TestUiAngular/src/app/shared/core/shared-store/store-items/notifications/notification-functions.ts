import * as _ from 'lodash';
import {NotificationModel, NotificationStore} from './notification-store';
import {AddNotificationsAction} from './notification-actions';

export function handleAddNotificationsAction(store: NotificationStore, action: AddNotificationsAction): NotificationStore {
  if (action.payload.notifications.length > 0) {

    const oldNotificationArray: NotificationModel[] = _.cloneDeep(store.oldNotifications);

    const newNotificationArray: NotificationModel[] = _.cloneDeep(action.payload.notifications);

    return new NotificationStore(oldNotificationArray.concat(newNotificationArray), newNotificationArray);
  }
  return store;
}
