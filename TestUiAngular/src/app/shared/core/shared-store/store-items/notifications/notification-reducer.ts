
import {NotificationStore} from './notification-store';
import {AddNotificationsAction, NotificationActions} from './notification-actions';
import {handleAddNotificationsAction} from './notification-functions';
import {nameofObject} from '../../../functions/nameof-operators';


export function NotificationReducer(store: NotificationStore, action: NotificationActions): NotificationStore {
  switch (action.type) {
    case  nameofObject(new AddNotificationsAction([])):
      return handleAddNotificationsAction(store, action);

    default:
      return store;
  }
}
