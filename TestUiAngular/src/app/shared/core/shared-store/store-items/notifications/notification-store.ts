

export class NotificationStore {
  constructor(
    public oldNotifications: NotificationModel[] = [],
    public newNotifications: NotificationModel[] = []
  ) {
  }
}



export class NotificationModel {
  constructor(
    public title: string,     // заголовок. Тут к примеру всегда Авторизация пользователя (по названию модуля)
    public message: string,   // текст сообщения
    public type: NotificationTypes       // тип сообщения. info/error
  ) {
  }
}

export enum NotificationTypes  {
  Info = 'info',
  Warning = 'warning',
  Error = 'error',
}
