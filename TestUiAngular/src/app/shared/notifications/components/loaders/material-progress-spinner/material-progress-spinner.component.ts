import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-material-progress-spinner',
  templateUrl: './material-progress-spinner.component.html',
  styleUrls: ['./material-progress-spinner.component.scss']
})
export class MaterialProgressSpinnerComponent implements OnInit, OnDestroy {

  @Input() showLoader$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  isLoaderShowed = false;

  // для убиваиня подписок
  private readonly ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor() {
  }

  ngOnInit() {
    this.showLoader$
      .pipe(takeUntil(this.ngUnsubscribe)
        // filter(x => x != null)
      )
      .subscribe(
        (res: boolean) => {
          this.isLoaderShowed = res;
        }
      );
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
