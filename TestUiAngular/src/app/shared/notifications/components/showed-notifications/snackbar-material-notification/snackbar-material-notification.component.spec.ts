import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnackbarMaterialNotificationComponent } from './snackbar-material-notification.component';

describe('SnackbarMaterialNotificationComponent', () => {
  let component: SnackbarMaterialNotificationComponent;
  let fixture: ComponentFixture<SnackbarMaterialNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnackbarMaterialNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnackbarMaterialNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
