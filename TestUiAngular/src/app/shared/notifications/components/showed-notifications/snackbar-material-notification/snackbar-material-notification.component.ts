import {Component, Input, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {BehaviorSubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AppNotificationModel} from '../../../models/app-notification-model';

@Component({
  selector: 'app-snackbar-material-notification',
  templateUrl: './snackbar-material-notification.component.html',
  styleUrls: ['./snackbar-material-notification.component.scss']
})
export class SnackbarMaterialNotificationComponent implements OnInit {

  @Input() appNotifications$: BehaviorSubject<AppNotificationModel[]> = new BehaviorSubject<AppNotificationModel[]>(null);


  // для убиваиня подписок
  private readonly ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    public snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {

    // this.snackBar.open('Test message', 'Test title', {
    //   duration: 2000,
    // });

    this.appNotifications$
      // .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (notifications: AppNotificationModel[]) => {

          // console.log('Должна пойти нотификация. notifications = ', notifications);

          if (notifications) {
            if (notifications.length > 0) {

              // console.log('Должна пойти нотификация - это после ифа ');

              // this.snackBar.open(notifications[0].message, notifications[0].title, {
              //   duration: 2000,
              // });

              notifications.forEach(
                (x: AppNotificationModel) => {
                  this.snackBar.open(x.message, x.title, {
                    duration: 2000,
                  });
                }
              );

            }
          }

        }
      );

  }


}
