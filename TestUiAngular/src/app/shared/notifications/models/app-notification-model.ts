import {NotificationModel, NotificationTypes} from '../../core/shared-store/store-items/notifications/notification-store';


export class AppNotificationModel implements NotificationModel {
  constructor(
    public title: string,     // заголовок. Тут к примеру всегда Авторизация пользователя (по названию модуля)
    public message: string,   // текст сообщения
    public type: NotificationTypes       // тип сообщения. info/error
  ) {
  }
}
