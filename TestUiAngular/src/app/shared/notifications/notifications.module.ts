import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatProgressSpinnerModule} from '@angular/material';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {SpinnerComponent} from './components/loaders/spinner/spinner.component';
import {NotificationComponent} from './components/showed-notifications/notification/notification.component';
import {SnackbarMaterialNotificationComponent} from './components/showed-notifications/snackbar-material-notification/snackbar-material-notification.component';
import {MaterialProgressSpinnerComponent} from './components/loaders/material-progress-spinner/material-progress-spinner.component';

@NgModule({
  imports: [
    CommonModule,

    MatProgressSpinnerModule,
    MatSnackBarModule
  ],
  declarations: [SpinnerComponent, NotificationComponent, SnackbarMaterialNotificationComponent, MaterialProgressSpinnerComponent],
  exports: [SpinnerComponent, NotificationComponent, SnackbarMaterialNotificationComponent, MaterialProgressSpinnerComponent]
})
export class NotificationsModule {
}
