import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WidgetsMaterialModule} from './widgets-material/widgets-material.module';

@NgModule({
  imports: [
    CommonModule
    , WidgetsMaterialModule
  ],
  exports: [WidgetsMaterialModule],
  declarations: []
})
export class WidgetsModule { }
