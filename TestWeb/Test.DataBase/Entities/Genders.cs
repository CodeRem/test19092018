﻿using System;
using System.Collections.Generic;

namespace Test.DataBase.Entities
{
    public partial class Genders
    {
        public Genders()
        {
            Persons = new HashSet<Persons>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }

        public ICollection<Persons> Persons { get; set; }
    }
}
