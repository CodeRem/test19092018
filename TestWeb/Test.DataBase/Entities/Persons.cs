﻿using System;
using System.Collections.Generic;

namespace Test.DataBase.Entities
{
    public partial class Persons
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalNumber { get; set; }
        public DateTime Birthdate { get; set; }
        public int GenderId { get; set; }
        public decimal Salary { get; set; }

        public Genders Gender { get; set; }
    }
}
