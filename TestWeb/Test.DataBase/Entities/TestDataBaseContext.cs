﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Test.DataBase.Entities
{
    public partial class TestDataBaseContext : DbContext
    {
        //public TestDataBaseContext()
        //{
        //}

        //public TestDataBaseContext(DbContextOptions<TestDataBaseContext> options)
        //    : base(options)
        //{
        //}

        public virtual DbSet<Genders> Genders { get; set; }
        public virtual DbSet<Persons> Persons { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Server=DESKTOP-085VGSP\\SQLEXPRESS;Database=TestDataBase;Trusted_Connection=True;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Genders>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(6);

                entity.Property(e => e.ShortName)
                    .IsRequired()
                    .HasMaxLength(1);
            });

            modelBuilder.Entity<Persons>(entity =>
            {
                entity.Property(e => e.Birthdate).HasColumnType("date");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PersonalNumber)
                    .IsRequired()
                    .HasMaxLength(11);

                entity.Property(e => e.Salary).HasColumnType("money");

                entity.HasOne(d => d.Gender)
                    .WithMany(p => p.Persons)
                    .HasForeignKey(d => d.GenderId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });
        }
    }
}
