﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test.DataBase.Enums
{
    public enum GendersEnum
    {
        Male = 1,
        Female = 2
    }
}
