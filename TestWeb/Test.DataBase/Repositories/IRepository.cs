﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Test.DataBase.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {

        #region Find Item

        TEntity FindById(int id);
        TEntity GetFirstOrDefault(Expression<Func<TEntity, bool>> expression);

        #endregion

        #region Find Item Async

        Task<TEntity> FindByIdAsync(int id);
        Task<TEntity> GetFirstOrDefaultAsync(Expression<Func<TEntity, bool>> expression);

        #endregion

        #region Filtration

        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate);

        #endregion

        #region CUD
        void Add(TEntity item);
        void AddRange(List<TEntity> listItems);
        void Update(TEntity item);
        void Delete(int itemId);
        #endregion

        #region CUD Async

        Task AddAsync(TEntity item);

        Task DeleteAsync(int itemId);

        #endregion

        #region Сохранение в БД Async

        Task SaveChangeAsync();

        #endregion

    }
}
