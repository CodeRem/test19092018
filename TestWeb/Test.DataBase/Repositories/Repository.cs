﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Test.DataBase.Entities;

namespace Test.DataBase.Repositories
{
    public class Repository<TEntity> : DataBase.Repositories.IRepository<TEntity> where TEntity : class
    {
        private TestDataBaseContext Context;
        private DbSet<TEntity> DbSet;

        public Repository(TestDataBaseContext context)
        {
            Context = context;
            DbSet = Context.Set<TEntity>();

            if (Context == null)
                throw new InvalidOperationException("DbContext is null.");
        }



        public virtual void Add(TEntity item)
        {
            DbSet.Add(item);
        }

        public void AddRange(List<TEntity> listItems)
        {
            DbSet.AddRange(listItems);
        }

        public virtual async Task AddAsync(TEntity item)
        {
            await DbSet.AddAsync(item);
        }

        public virtual void Delete(int itemId)
        {
            var item = DbSet.Find(itemId);
            DbSet.Remove(item);
        }

        public virtual async Task DeleteAsync(int itemId)
        {
            var item = await DbSet.FindAsync(itemId);
            DbSet.Remove(item);
        }

        public async Task SaveChangeAsync()
        {
            await Context.SaveChangesAsync();
        }

        public virtual TEntity FindById(int id)
        {
            return DbSet.Find(id);
        }

        public virtual async Task<TEntity> FindByIdAsync(int id)
        {
            return await DbSet.FindAsync(id);
        }

        public IQueryable<TEntity> GetAll()
        {
            return Context.Set<TEntity>();
        }

        public TEntity GetFirstOrDefault(Expression<Func<TEntity, bool>> expression)
        {
            return DbSet.FirstOrDefault(expression);
        }

        public virtual async Task<TEntity> GetFirstOrDefaultAsync(Expression<Func<TEntity, bool>> expression)
        {
            return await DbSet.FirstOrDefaultAsync(expression);
        }

        public void Update(TEntity item)
        {
            Context.Entry(item).State = EntityState.Modified;
        }

        public IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Where(predicate);
        }


    }
}
