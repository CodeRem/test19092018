﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Test.Domain.Dto.Genders
{
    public class GenderDto
    {
        public static Expression<Func<DataBase.Entities.Genders, GenderDto>> Projection
        {
            get
            {
                return p => new GenderDto
                {
                    Id = p.Id,
                    Name = p.Name,
                    ShortName = p.ShortName
                };
            }
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }
}
