﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Test.Domain.Dto.Persons
{
    public class PersonDto
    {

        public static Expression<Func<DataBase.Entities.Persons, PersonDto>> Projection
        {
            get
            {
                return p => new PersonDto
                {
                    Id = p.Id,
                    FirstName = p.FirstName,
                    LastName = p.LastName,
                    PersonalNumber = p.PersonalNumber,
                    Birthdate = p.Birthdate,
                    GenderId = p.GenderId,
                    Gender = p.Gender.Name,
                    Salary = p.Salary
                }; 
            }
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalNumber { get; set; }
        public DateTime Birthdate { get; set; }
        public int GenderId { get; set; }
        public string Gender { get; set; }
        public decimal Salary { get; set; }
    }
}
