﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Test.DataBase.Repositories;
using Test.Domain.Dto.Genders;

namespace Test.Domain.Services.Genders
{
    public class GendersDomainService: IGendersDomainService
    {
        private readonly IRepository<DataBase.Entities.Genders> _repository;

        public GendersDomainService(IRepository<DataBase.Entities.Genders> repository)
        {
            _repository = repository;
        }

        public async Task<List<GenderDto>> ListGenderDto()
        {
            return await _repository.GetAll().Select(GenderDto.Projection).ToListAsync();
        }
    }
}
