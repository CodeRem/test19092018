﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Test.Domain.Dto.Genders;

namespace Test.Domain.Services.Genders
{
    public interface IGendersDomainService
    {
        Task<List<GenderDto>> ListGenderDto();
    }
}
