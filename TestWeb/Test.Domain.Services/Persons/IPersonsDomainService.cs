﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Test.Domain.Dto.Persons;

namespace Test.Domain.Services.Persons
{
    public interface IPersonsDomainService
    {
        Task<List<PersonDto>> ListAllPersonsAsync();
        Task CreatePerson(PersonCreateDto dto);
        Task UpdatePerson(PersonUpdateDto dto);
        Task DeletePerson(int id);
    }
}
