﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.DataBase.Repositories;
using Test.Domain.Dto.Persons;

namespace Test.Domain.Services.Persons
{
    public class PersonsDomainService : IPersonsDomainService
    {
        private readonly IRepository<DataBase.Entities.Persons> _personsRepository;

        public PersonsDomainService(IRepository<DataBase.Entities.Persons> personsRepository)
        {
            _personsRepository = personsRepository;
        }

        public async Task<List<PersonDto>> ListAllPersonsAsync()
        {
            return await _personsRepository.GetAll().Select(PersonDto.Projection).ToListAsync();
        }

        public async Task CreatePerson(PersonCreateDto dto)
        {
            var entity = new DataBase.Entities.Persons
            {
                Id = 0,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                Birthdate = dto.Birthdate,
                PersonalNumber = dto.PersonalNumber,
                Salary = dto.Salary,
                GenderId = (int) dto.GenderId
            };

            await _personsRepository.AddAsync(entity);
            await _personsRepository.SaveChangeAsync();
        }

        public async Task UpdatePerson(PersonUpdateDto dto)
        {
            var entity = await _personsRepository.FindByIdAsync(dto.Id);

            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.Birthdate = dto.Birthdate;
            entity.PersonalNumber = dto.PersonalNumber;
            entity.Salary = dto.Salary;
            entity.GenderId = (int) dto.GenderId;

            _personsRepository.Update(entity);

            await _personsRepository.SaveChangeAsync();
        }

        public async Task DeletePerson(int id)
        {
            await _personsRepository.DeleteAsync(id);
            await _personsRepository.SaveChangeAsync();
        }
    }
}
