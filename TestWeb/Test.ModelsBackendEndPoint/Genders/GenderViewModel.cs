﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test.ModelsBackendEndPoint.Genders
{
    public class GenderViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
