﻿using System;
using System.Collections.Generic;
using System.Text;
using Test.DataBase.Enums;

namespace Test.ModelsBackendEndPoint.Persons
{
    public class PersonUpdateViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalNumber { get; set; }
        public DateTime Birthdate { get; set; }
        public GendersEnum GenderId { get; set; }
        public decimal Salary { get; set; }
    }
}
