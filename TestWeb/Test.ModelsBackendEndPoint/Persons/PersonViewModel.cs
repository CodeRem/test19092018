﻿using System;
using System.Collections.Generic;
using System.Text;
using Test.Domain.Dto.Persons;
using Test.ModelsBackendEndPoint.Genders;

namespace Test.ModelsBackendEndPoint.Persons
{
    public class PersonViewModel
    {
        public PersonViewModel(PersonDto dto)
        {
            Id = dto.Id;
            FirstName = dto.FirstName;
            LastName = dto.LastName;
            PersonalNumber = dto.PersonalNumber;
            Birthdate = dto.Birthdate;
            Gender = new GenderViewModel
            {
                Id = dto.GenderId,
                Name = dto.Gender
            };
            Salary = dto.Salary;
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalNumber { get; set; }
        public DateTime Birthdate { get; set; }
        public GenderViewModel Gender { get; set; }
        public decimal Salary { get; set; }
    }
}
