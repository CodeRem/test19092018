﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Test.DataBase.Repositories;
using Test.Domain.Services.Genders;
using Test.Domain.Services.Persons;

namespace Test.Services.DomainServicesConfigDi
{
    public class ConfigureDomainServicesStartup
    {
        public ConfigureDomainServicesStartup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public void ConfigureService(IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

            services.AddScoped<IPersonsDomainService, PersonsDomainService>();
            services.AddScoped<IGendersDomainService, GendersDomainService>();

        }
    }
}
