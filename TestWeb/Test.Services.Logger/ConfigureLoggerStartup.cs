﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Test.Services.Logger
{
    public class ConfigureLoggerStartup
    {
        public ConfigureLoggerStartup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public void Configure(ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();
        }
    }
}
