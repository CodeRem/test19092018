﻿using System;
using System.Collections.Generic;
using System.Text;
using Serilog;
using Serilog.Events;
using Serilog.Exceptions;
using Serilog.Formatting.Json;

namespace Test.Services.Logger
{
    public class LoggerService
    {
        public static void LoggerConfiguration()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Error)
                .MinimumLevel.Override("System", LogEventLevel.Error)
                .MinimumLevel.Override("Default", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .Enrich.WithExceptionDetails()
                .Enrich.WithMachineName()
                .WriteTo.Async(x => x.RollingFile(new JsonFormatter(), "logs/{Date}.log"))
                .CreateLogger();
        }
    }
}
