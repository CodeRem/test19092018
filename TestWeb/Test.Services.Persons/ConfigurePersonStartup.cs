﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Test.Services.Persons.GenderService;
using Test.Services.Persons.PersonsService;

namespace Test.Services.Persons
{
    public class ConfigurePersonStartup
    {
        public ConfigurePersonStartup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public void ConfigureService(IServiceCollection services)
        {
            services.AddScoped <IPersonsService, PersonsService.PersonsService> ();
            services.AddScoped<IGenderService, GenderService.GenderService>();
        }
    }
}
