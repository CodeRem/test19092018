﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Domain.Services.Genders;
using Test.ModelsBackendEndPoint.Genders;

namespace Test.Services.Persons.GenderService
{
    public class GenderService: IGenderService
    {
        private readonly IGendersDomainService _gendersDomainService;

        public GenderService(IGendersDomainService gendersDomainService)
        {
            _gendersDomainService = gendersDomainService;
        }

        public async Task<List<GenderViewModel>> ListGenders()
        {
            return (await _gendersDomainService.ListGenderDto()).Select(x => new GenderViewModel
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
        }
    }
}
