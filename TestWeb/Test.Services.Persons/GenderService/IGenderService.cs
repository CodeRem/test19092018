﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Test.ModelsBackendEndPoint.Genders;

namespace Test.Services.Persons.GenderService
{
    public interface IGenderService
    {
        Task<List<GenderViewModel>> ListGenders();
    }
}
