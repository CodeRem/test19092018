﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Test.ModelsBackendEndPoint.Persons;

namespace Test.Services.Persons.PersonsService
{
    public interface IPersonsService
    {
        Task<List<PersonViewModel>> ListAllPersonsAsync();
        Task CreatePerson(PersonCreateViewModel model);
        Task UpdatePerson(PersonUpdateViewModel model);
        Task DeletePerson(int id);
    }
}
