﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Domain.Dto.Persons;
using Test.Domain.Services.Persons;
using Test.ModelsBackendEndPoint.Persons;

namespace Test.Services.Persons.PersonsService
{
    public class PersonsService: IPersonsService
    {
        private readonly IPersonsDomainService _personsDomainService;

        public PersonsService(IPersonsDomainService personsDomainService)
        {
            _personsDomainService = personsDomainService;
        }

        public async Task<List<PersonViewModel>> ListAllPersonsAsync()
        {
            return (await _personsDomainService.ListAllPersonsAsync()).Select(x => new PersonViewModel(x)).ToList();
        }

        public async Task CreatePerson(PersonCreateViewModel model)
        {
            var personCreateDto = new PersonCreateDto
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Birthdate = model.Birthdate,
                PersonalNumber = model.PersonalNumber,
                GenderId = model.GenderId,
                Salary = model.Salary
            };

            await _personsDomainService.CreatePerson(personCreateDto);
        }

        public async Task UpdatePerson(PersonUpdateViewModel model)
        {
            var personUpdateDto = new PersonUpdateDto
            {
                Id = model.Id,
                FirstName = model.FirstName,
                LastName = model.LastName,
                PersonalNumber = model.PersonalNumber,
                Birthdate = model.Birthdate,
                Salary = model.Salary,
                GenderId = model.GenderId
            };

            await _personsDomainService.UpdatePerson(personUpdateDto);
        }

        public async Task DeletePerson(int id)
        {
            await _personsDomainService.DeletePerson(id);
        }
    }
}
