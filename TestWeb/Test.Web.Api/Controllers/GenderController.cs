﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Test.Services.Persons.GenderService;
using Test.Web.Api.Extensions;

namespace Test.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenderController : ControllerBase
    {
        private readonly ILogger<GenderController> _logger;
        private readonly IGenderService _genderService;

        public GenderController(ILogger<GenderController> logger, IGenderService genderService)
        {
            _logger = logger;
            _genderService = genderService;
        }

        [HttpGet]
        [Route(nameof(GetListGenders))]
        public async Task<IActionResult> GetListGenders()
        {
            try
            {
                var result = await _genderService.ListGenders();
                return Ok(result);
            }
            catch (Exception e)
            {
                return this.ExceptionActions(e, _logger, nameof(GetListGenders));
            }
        }

    }
}