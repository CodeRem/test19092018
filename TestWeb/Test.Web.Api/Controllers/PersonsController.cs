﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Test.ModelsBackendEndPoint.Persons;
using Test.Services.Persons.PersonsService;
using Test.Web.Api.Extensions;

namespace Test.Web.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class PersonsController : ControllerBase
    {
        private readonly ILogger<PersonsController> _logger;
        private readonly IPersonsService _personsService;

        public PersonsController(ILogger<PersonsController> logger, IPersonsService personsService)
        {
            _logger = logger;
            _personsService = personsService;
        }

        [HttpGet]
        [Route(nameof(GetListPersons))]
        public async Task<IActionResult> GetListPersons()
        {
            try
            {
                var result = await _personsService.ListAllPersonsAsync();
                return Ok(result);
            }
            catch (Exception e)
            {
                return this.ExceptionActions(e, _logger, nameof(GetListPersons));
            }
        }

        [HttpPost]
        [Route(nameof(CreatePerson))]
        public async Task<IActionResult> CreatePerson([FromBody] PersonCreateViewModel model)
        {
            try
            {
                await _personsService.CreatePerson(model);
                return Ok();
            }
            catch (Exception e)
            {
                return this.ExceptionActions(e, _logger, nameof(CreatePerson));
            }
        }

        [HttpPut]
        [Route(nameof(UpdatePerson))]
        public async Task<IActionResult> UpdatePerson([FromBody] PersonUpdateViewModel model)
        {
            try
            {
                await _personsService.UpdatePerson(model);
                return Ok();
            }
            catch (Exception e)
            {
                return this.ExceptionActions(e, _logger, nameof(UpdatePerson));
            }
        }

        [HttpDelete]
        [Route(nameof(DeletePerson) + "/{id}")]
        public async Task<IActionResult> DeletePerson(int id)
        {
            try
            {
                await _personsService.DeletePerson(id);
                return Ok();

            }
            catch (Exception e)
            {
                return this.ExceptionActions(e, _logger, nameof(DeletePerson));
            }
        }
    }
}