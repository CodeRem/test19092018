﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Test.Web.Api.Extensions
{
    public static class ControllerExtension
    {
        public static IActionResult ExceptionActions(this Controller controller, Exception e, ILogger logger, string action)
        {
            logger.LogError(e, $"Error in controller {nameof(controller)}/{action}");
            return controller.StatusCode(500);
        }

        public static IActionResult ExceptionActions(this ControllerBase controller, Exception e, ILogger logger, string action)
        {
            logger.LogError(e, $"Error in controller {nameof(controller)}/{action}");
            return controller.StatusCode(500);
        }
    }
}
