﻿using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Test.DataBase.Entities;
using Test.Services.DomainServicesConfigDi;
using Test.Services.Logger;
using Test.Services.Persons;

namespace Test.Web.Api
{
    public class Startup
    {
        private readonly ConfigureLoggerStartup _configureLoggerStartup;
        private readonly ConfigureDomainServicesStartup _configureDomainServicesStartup;
        private readonly ConfigurePersonStartup _configurePersonStartup;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _configureLoggerStartup = new ConfigureLoggerStartup(configuration);
            _configureDomainServicesStartup = new ConfigureDomainServicesStartup(configuration);
            _configurePersonStartup = new ConfigurePersonStartup(configuration);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TestDataBaseContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // DI generic repositories and domain services
            _configureDomainServicesStartup.ConfigureService(services);

            // Persons Services
            _configurePersonStartup.ConfigureService(services);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            // логгер 
            _configureLoggerStartup.Configure(loggerFactory);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                // включаем КОРС для крос доменных запросов для разработки (под WebStorm, port 4200).
                app.UseCors(builder =>
                    builder.WithOrigins("http://localhost:4200")
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()
                );
            }

            // обрабатываем запросы - все кроме запросов на .../api и на статические файлы - редиректим на index.html https://code.msdn.microsoft.com/How-to-fix-the-routing-225ac90f
            app.Use(async (context, next) =>
                {
                    await next();
                    if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value))
                    {
                        context.Request.Path = "/index.html";
                        await next();
                    }
                })
                .UseDefaultFiles(new DefaultFilesOptions { DefaultFileNames = new List<string> { "index.html" } })
                .UseStaticFiles()
                .UseMvc();
        }
    }
}
